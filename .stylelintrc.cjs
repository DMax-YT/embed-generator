module.exports = {
  extends: 'stylelint-config-standard',
  rules: {
    'selector-class-pattern': ['^[a-z][a-zA-Z0-9]+$', {
      message: 'Class should be named in camelCase'
    }],
    'declaration-empty-line-before': null,
    'color-hex-length': null,
    'import-notation': null
  },
  ignoreFiles: ['**/normalize.css']
}
