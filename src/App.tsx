const App = () => {
  return (
    <div class="content">
      Discord Embed Generator

      <footer>
        Icons provided by <a target="_blank" href="https://icons8.com">Icons8</a>
      </footer>
    </div>
  )
}

export { App }
