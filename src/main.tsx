import { render } from 'preact'

import './normalize.css'
import { App } from './App'

render(<App />, document.getElementById('app') as HTMLElement)
