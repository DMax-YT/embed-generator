import { defineConfig } from 'vite'
import preact from '@preact/preset-vite'
import eslint from 'vite-plugin-eslint'
import stylelint from 'vite-plugin-stylelint'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    preact(),
    stylelint(),

    {
      ...eslint(),
      apply: 'build'
    },
    {
      ...eslint({
        failOnWarning: false,
        failOnError: false
      }),
      apply: 'serve',
      enforce: 'post'
    }
  ]
})
